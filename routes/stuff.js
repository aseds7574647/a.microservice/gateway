const express = require('express');
const router = express.Router();

const apiCtrl = require('../controllers/stuff');
const auth = require('../middleware/auth')

router.get('/getOneThing/:id', apiCtrl.getOneThing);
router.get('/getAllThings', apiCtrl.getAllStuff);

router.post('/addOrder', auth, apiCtrl.ajouterCommande);
router.post('/updateOrder/:id', apiCtrl.updateCommande);

router.post('/payOrder', auth, apiCtrl.payerUneCommande);

router.post('/register', apiCtrl.register);
router.post('/login', apiCtrl.login);

module.exports = router;