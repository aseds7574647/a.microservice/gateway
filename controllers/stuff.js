axios = require('axios');

exports.getOneThing = (req, res, next) => {
    try {
        axios.get(`http://host.docker.internal:3005/produits/${req.params.id}`)
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                console.error(error);
            })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.getAllStuff = (req, res, next) => {
    try {
        axios.get(`http://host.docker.internal:3005/produits/`)
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                console.error(error);
            })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.ajouterCommande = (req, res) => {
    try {
        axios.post(`http://host.docker.internal:3006/commande`, { title: req.body.title })
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                console.error(error);
            })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.updateCommande = (req, res) => {
    try {
        console.log(req.params.id);
        axios.put(`http://host.docker.internal:3006/commande/${req.params.id}`, { commandePayee: true })
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                console.error(error);
            })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.payerUneCommande = async (req, res) => {
    try {
        await axios.post(`http://host.docker.internal:3007/paiement`, { orderId: req.body.idCommande })
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                console.error(error);
            });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.register = async (req, res) => {
    try {
        await axios.post(`http://host.docker.internal:3008/auth/register`, {
            email: req.body.email,
            password: req.body.password
        })
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                res.status(400).json(error.response.data);
            })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.login = (req, res, next) => {
    try {
        axios.post(`http://host.docker.internal:3008/auth/login`, {
            email: req.body.email,
            password: req.body.password
        })
            .then(products => {
                res.json(products.data);
            })
            .catch(error => {
                // Retournez un message d'erreur approprié
                res.status(401).json({ error: 'Email ou mot de passe invalide' });
            })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};